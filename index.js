/*This file contains the main server code. It requires a wrapper, such as the
  cli.js file included with the program.
*/
const http = require("http"),
	https = require("https"),
	url = require("url"),
	path = require("path"),
	fs = require("fs"),
	mime = require("mime"),
	parseQuery = function (query){
		let options = query.split('&'),
			optionsArr = [],
			result = {};
		for(i[0] = 0; i[0] < options.length; i[0]++){
			optionsArr.push(options[i[0]].split('='));
		}
		for(i[0] = 0; i[0] < optionsArr.length; i[0]++){
			let key = optionsArr[i[0]][0],
				val = optionsArr[i[0]][1];
			result = Object.assign(result, {[key]: val});
		}
		return result
	},
	staticFile = (request, response) => {
		const serve = (status, filename, callback) => {
			fs.readFile(filename, "binary", function(error, file) {
				if(error) {
					callback (error);
				}
				else {
					response.writeHead(status, Object.assign({"Content-Type": mime.getType(filename)}, config.headers));
					response.write(file, "binary");
					response.end();
					callback();
				}
			});
		}
		if(typeof module.exports.onRequest == "function"){
			module.exports.onRequest(request, response);
		}
		try {
			let uri = url.parse(request.url).pathname,
				filename = path.join(config.files.root, uri);
			if(config.api && (
				config.api == uri ||
				'/' + config.api == uri
			)){
				if(typeof module.exports.onAPICall == "function"){
					module.exports.onAPICall(uri, parseQuery(url.parse(request.url).query), request, response);
				}
			} else {
				if(typeof module.exports.onStaticRequest == "function"){
					module.exports.onStaticRequest(request, response);
				}
				serve(200, filename, error => {
					if (error) {
						serve(200, path.join(filename, config.files.index), error => {
							if (error) {
								serve(200, filename + config.files.defext, error => {
									if(error) {
										if(typeof module.exports.onError404 == "function"){
											module.exports.onError404(request, response);
										}
										serve(404, path.join(config.files.root, config.files.notFound), error => {
											if (error) {
												response.writeHead(404, {"Content-Type": "text/plain"});
												response.write("404 not found");
												response.end();
											}
										});
									}
								});
							}
						});
					}
				});
			}
		}
		catch(error){
			if(typeof module.exports.onError500 == "function"){
				module.exports.onError500();
			}
			serve(500, path.join(config.files.root, config.files.error), error => {
				if (error) {
					response.writeHead(500, {"Content-Type": "text/plain"});
					response.write("An internal server error has occurred.");
					response.end();
				}
			});
		}
	};

let config, HTTPServer, HTTPSServer;
module.exports.startHTTPServer = function(configParam){
	try{
		config = configParam;
		if(config.mode == "httpsredir" || config.mode == "httpsonly" || config.mode == "httphttps"){
			HTTPSServer = https.createServer({
				key: fs.readFileSync(config.crypto.key),
				cert: fs.readFileSync(config.crypto.cert),
			}, staticFile).listen(config.porthttps);
		}
		if(config.mode == "httponly" || config.mode == "httphttps"){
			HTTPServer = http.createServer(staticFile).listen(config.porthttp);
		}
		if(config.mode == "httpsredir"){
			HTTPServer = http.createServer((request, response) => {
				response.writeHead(302, {"Location": `https://${request.headers.host}${request.url}`});
				response.end();
			}).listen(config.porthttp);
		}
		if(typeof module.exports.onServerStart == "function"){
			module.exports.onServerStart();
		}
	}
	catch (error) {
		if(typeof module.exports.onServerFatalError == "function"){
			module.exports.onServerFatalError(error);
		}
	}
};
module.exports.configureHTTPServer = function(configParam, restartServer){
	if(HTTPServer || HTTPSServer){
		config = configParam;
	} else {
		throw "configureHTTPServer method is meant to configure an already-running HTTP/S server, possibly restarting it. There is no server running."
	}
	if(restartServer){
		module.exports.stopHTTPServer();
		module.exports.startHTTPServer(config);
	}
};
module.exports.stopHTTPServer = function(){
	try{
		HTTPServer.close();
		HTTPSServer.close();
	} finally{
		HTTPServer = undefined;
		HTTPSServer = undefined;
		if(typeof module.exports.onServerStop == "function"){
			module.exports.onServerStop();
		}
	}
}

module.exports.onRequest = function(){};
module.exports.onAPICall = function(){};
module.exports.onStaticRequest = function(){};
module.exports.onError404 = function(){};
module.exports.onError500 = function(){};
module.exports.onServerStart = function(){};
module.exports.onServerFatalError = function(){};
module.exports.onServerStop = function(){};
