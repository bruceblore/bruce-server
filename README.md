# Bruce Server

A very simple to use and somewhat configurable HTTP(S) server (named after me) in node.js that can be run on the command line or `require()`d, with the ability to easily build an HTTP(S) API on top of it.

# Getting started

This program can be installed with `npm install bruce-server`. It can be `require()`d or used as a command line program. Sample code:
```javascript
const httpServer = require("bruce-server");
httpServer.startHTTPServer({
	files: {
		index: "index.html",
		notFound: "404.html",
		error: "500.html",
		defext: ".html",
		root: process.cwd(),
	},
	crypto: {
		key: path.join(process.env.HOME, "key.pem"),
		cert: path.join(process.env.HOME, "cert.pem")
	},
	porthttp: 8080,
	porthttps: 8443,
	mode: "httpsredir",
	api: undefined
});
```

It is mandatory to pass a configuration object as the first parameter. The sample code includes a reasonable default, but you may wish to modify it. See the docs for more details.

To run on the command line, type `bruce-server`. If it is used that way, it will default to a configuration of
```javascript
{
	files: {
		index: "index.html",
		notFound: "404.html",
		error: "500.html",
		defext: ".html",
		root: process.cwd()
	},
	crypto: {
		key: path.join(process.env.HOME, "key.pem"),
		cert: path.join(process.env.HOME, "cert.pem")
	},
	porthttp: 8080,
	porthttps: 8443,
	mode: "httpsredir",
	api: undefined
}
```
and will then try to read from a JSON file at ~/.bruceserverrc, then ./.bruceserverrc. If a command line argument is passed, the server will first attempt to parse it as configuration JSON, then as a location of a file containing configuration JSON. Invalid configuration is ignored. When launched this way, the server produces a log file at ~/.bruceserverlog.

## Contributing

The Git repository is at https://gitlab.com/0100001001000010/bruce-server. The issue tracker is at https://gitlab.com/0100001001000010/bruce-server/issues. All contributions are welcome. Please check out a new branch with a memorable name, such as your username, to make your contributions on.

The master branch contains the releases. You probably should not base your branch on this one.

The development branch is where most development takes place. You probably should base your branch on this one.

## Dependencies

node.js (Tested on 8.11.1) - JavaScript runtime

fs 0.0.1-security - read files, write the log file

http 0.0.0 - serve stuff over HTTP

https ^1.0.0 - serve stuff over HTTPS

mime ^2.3.1 - determine the MIME type of files for the HTTP header

path ^0.12.7 - deal with file paths

url ^0.11.0 - parse URLs

nodemon ^1.18.3 (devDependencies only) - restart when source code changes

## Licensing

This code is licensed under the GNU GPL version 3. You should have received a copy of the license with this program.

## Disclaimer

The license says it, but I'll say it again. This program comes with no warranty. This means that you use it at your own risk and I am not responsible for anything that happens as a result of using this program.

This is something I put together in my free time for fun. I have done my best to make it stable and bug free, but I do not guarantee that it is. Before using my program for something important, please test it.
