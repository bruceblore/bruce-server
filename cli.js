#!/usr/bin/env node
//This forms the command line wrapper for the server.
const httpServer = require("./index.js"),
	path = require("path"),
	fs = require("fs"),
	logger = require("./logger.js");

logger.startLogging();
logger.log("Server started!", true);
let config, configHome, configPWD, configCMDLine,
	configDefault = {
		files: {
			index: "index.html",
			notFound: "404.html",
			error: "500.html",
			defext: ".html",
			root: process.cwd(),
		},
		crypto: {
			key: path.join(process.env.HOME, "key.pem"),
			cert: path.join(process.env.HOME, "cert.pem")
		},
		headers: {
			"Content-Security-Policy": "default-src 'none'; script-src 'self'; connect-src 'self'; img-src 'self'; style-src 'self';"
		},
		porthttp: 8080,
		porthttps: 8443,
		mode: "httpsredir",
		api: undefined
	},
	onShutdown = function(){
		logger.log("Shutting down...", true);
		httpServer.stopHTTPServer();
		logger.log("Shutdown successful.", true);
		logger.stopLogging();
		process.exit(0);
	};

	httpServer.onServerStart = function(){
		logger.log("Server started successfully.", true);
	}
	httpServer.onServerFatalError = function(error){
		logger.log("ERROR: The server could not start or has crashed. See details below.", "stderr");
		logger.log(JSON.stringify(error), "stderr");
		process.exit(1);
	}
	httpServer.onAPICall = function(uri, query, request, response){
		logger.log(`API Call on ${uri} with query ${JSON.stringify(query)}.`);
	};
	httpServer.onStaticRequest = function(request, response){
		logger.log("Static request for " + request.url);
	};
	httpServer.onError500 = function(){
		logger.log("WARNING: A 500 error has occured.", "stderr");
		logger.log(JSON.stringify(global), "stderr");
	}

	process.on("SIGINT", onShutdown);
	process.on("SIGTERM", onShutdown);
	process.on("SIGHUP", onShutdown);
	process.on("uncaughtException", function(error){
		logger.log("ERROR: The server has crashed due to an unhandled exception. This most likely indicates a bug.", "stderr");
		logger.log(JSON.stringify(error), "stderr");
		logger.log("Attempting a clean shutdown...");
		httpServer.stopHTTPServer();
		logger.stopLogging();
		process.exit(1);
	});

try {
	logger.log("Attempting to load configuration from home directory.")
	configHome = JSON.parse(fs.readFileSync(path.join(process.env.HOME, ".bruceserverrc")));
}
catch (error) {
	logger.log("WARNING: The config file in the home directory is missing, inaccessible, or contains errors.", "stderr");
	logger.log("It is recommended to configure the server using a JSON file at ~/.bruceserverrc.", "stderr");
	configHome = {};
}
try {
	logger.log("Attempting to load configuration from working directory.");
	configPWD = JSON.parse(fs.readFileSync(path.join(process.cwd(), ".bruceserverrc")));
	logger.log("WARNING: A configuration file has been detected in the current working directory.", "stderr");
	logger.log("It is recommended to configure the server using a JSON file at ~/.bruceserverrc unless you need a different configuration in some situations.", "stderr");
}
catch (error) {configPWD = {};}
try {
	try {
		logger.log("Attempting to parse command line arguments as configuration JSON");
		configCMDLine = JSON.parse(process.argv[2]);
	} catch (error) {
		logger.log("Attempting to parse command line arguments as file containing configuration JSON");
		configCMDLine = JSON.parse(fs.readFileSync(process.argv[2]));
	}
	logger.log("WARNING: It is not recommended to configure the server using the command line because you are probably manually typing the configuration every time or typing it in an unreadable form in a shell script.", "stderr");
	logger.log("The server checks for configuration in ./.bruceserverrc and ~/.bruceserverrc. It is recommended to use the home directory wherever possible.", "stderr");
}
catch (error) {
	configCMDLine = {};
	if(process.argv[2]){
		logger.log("WARNING: Text has been passed on the command line, The configuration on the command line is not valid JSON, or does not point to a readable file containing valid JSON. It has been ignored.", "stderr");
	}
}
config = Object.assign(configDefault, configHome, configPWD, configCMDLine);
logger.log("Current server configuration: " + JSON.stringify(config), true);

if ((config.files.root.indexOf("home") < 2 && config.files.root.indexOf("home") != -1) || (config.files.root.indexOf("user") < 4 && config.files.root.indexOf("user") != -1)){
	logger.log("WARNING: It appears that you are serving files out of your home directory. Please make sure that you are not serving files that you don't want to serve.", "stderr");
}
if (process.getuid == 0){
	logger.log("DANGER: You are running the server as root.")
	console.warn("                                 !!! DANGER !!!");
	console.warn("  You are running this program as root. In the event of a security flaw in");
	console.warn("    your program, my server library, or node.js, your entire system might be");
	console.warn("        compromised. If you would like to stop the server, press Ctrl+C.");
}

httpServer.startHTTPServer(config);
